package com.example.schornisko

import com.example.schornisko.model.Animal
import com.example.schornisko.repository.ShelterRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class Controller(
        val shelterRepository: ShelterRepository
) {

    @GetMapping("/shelter/all")
    fun getAllShelters(): List<Animal> {
        return shelterRepository.findAll()
    }

    @PostMapping("/shelter/add")
    fun addShelter(@RequestBody animal: Animal): Animal {
        return shelterRepository.save(animal)
    }

    @GetMapping("/create_data_base")
    fun createDataBase() {
        shelterRepository.saveAll(DataProvider.getSampleAnimals())
    }


}