package com.example.schornisko.model

enum class AnimalType {
    CAT, DOG
}