package com.example.schornisko.model

import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
data class Animal(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long?,
        val name: String,
        val age: Int,
        @Enumerated(EnumType.STRING)
        val sex: Sex,
        val address: String,
        @Enumerated(EnumType.STRING)
        val animalType: AnimalType,

        )
