package com.example.schornisko.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id


@Entity
data class ShelterParameters(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long?,
        val name: String,
        val adress: String,
        val phoneNumber: String,
        val city: String,
        val postCode: String,
        val adressURL: String,
        val bankAccount: String,
        )