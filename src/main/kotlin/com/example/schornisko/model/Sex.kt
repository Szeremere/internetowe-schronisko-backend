package com.example.schornisko.model

enum class Sex {
    MALE, FEMALE
}