package com.example.schornisko.model

import java.awt.Image
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
data class Actions(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val feed: String,
        val play: String,
        val walk: String,
        val water: String,
)
