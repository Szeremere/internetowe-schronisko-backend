package com.example.schornisko

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SchorniskoApplication

fun main(args: Array<String>) {
    runApplication<SchorniskoApplication>(*args)
}
