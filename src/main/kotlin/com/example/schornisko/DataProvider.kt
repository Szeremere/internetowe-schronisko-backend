package com.example.schornisko

import com.example.schornisko.model.Animal
import com.example.schornisko.model.AnimalType
import com.example.schornisko.model.Sex

class DataProvider {
    companion object {
        fun getSampleAnimals(): List<Animal> {
            return listOf(Animal(name = "Mruczek", age = 5, sex = Sex.MALE, address = "Jasna 5", animalType = AnimalType.CAT))
        }
    }
}