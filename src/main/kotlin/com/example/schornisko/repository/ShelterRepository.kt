package com.example.schornisko.repository

import com.example.schornisko.model.Animal
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ShelterRepository: JpaRepository<Animal, Long>